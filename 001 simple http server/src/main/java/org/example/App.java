package org.example;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main( String[] args ) throws Exception {
        try (ServerSocket serverSocket = new ServerSocket(8080)) {
            while (true) {
                try (Socket client = serverSocket.accept()) {
                    handleClient(client);
                }
            }
        }
    }

    private static void handleClient(Socket client) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(client.getInputStream());

        int i =0;
        StringBuilder requestBuilder = new StringBuilder();
        while ((i = bis.read()) != -1) {
            char c = (char) i;
            requestBuilder.append(c);

            if (requestBuilder.toString().endsWith("\r\n\r\n")) {
                break;
            }
        }

        String request = requestBuilder.toString();
        String[] requestsLines = request.split("\r\n");
        String[] requestLine = requestsLines[0].split(" ");
        String method = requestLine[0];
        String path = requestLine[1];
        String version = requestLine[2];
        String host = requestsLines[1].split(" ")[1];

        List<String> headers = new ArrayList<>();
        for (int h = 2; h < requestsLines.length; h++) {
            String header = requestsLines[h];
            headers.add(header);
        }

        String accessLog = String.format("Client %s, method %s, path %s, version %s, host %s, headers %s",
                client.toString(), method, path, version, host, headers.toString());
        System.out.println(accessLog);

        Path filePath = getFilePath(path);

        if (Files.exists(filePath)) {
            // file exist
            String contentType = guessMimeType(filePath);
            OutputStream clientOutput = client.getOutputStream();
            clientOutput.write("HTTP/1.1 200 OK\r\n".getBytes());
            clientOutput.write(("ContentType: " + contentType + "\r\n").getBytes());
            clientOutput.write("\r\n".getBytes());
            clientOutput.write(Files.readAllBytes(filePath));
            clientOutput.write("\r\n\r\n".getBytes());
            clientOutput.flush();
            client.close();
        } else {
            // 404
            OutputStream clientOutput = client.getOutputStream();
            clientOutput.write("HTTP/1.1 404 Not Found\r\n".getBytes());
            clientOutput.write("\r\n".getBytes());
            clientOutput.write("<h1>Not found :(</h1>".getBytes());
            clientOutput.write("\r\n\r\n".getBytes());
            clientOutput.flush();
            client.close();
        }


    }

    public static Path getFilePath(String path) {
        if ("/".equals(path)) {
            path = "/index.html";
        }

        return Paths.get("/tmp/www", path);
    }

    public static String guessMimeType(Path filePath) throws IOException {
        return Files.probeContentType(filePath);
    }

}
